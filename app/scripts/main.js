jQuery(document).ready(function($) {
  $('.slider').each(function(i,e) {
    $(e).slick({
      slidesToShow: 1,
      arrows: true,
      autoplay: true,
      dots: true,
      fade: true,
      infinite: false
    });
  });

  var chart1 = c3.generate({
      bindto: "#chart-1",
      data: {
          x: 'x',
          columns: [
              ['x', 1, 2, 3, 4, 5, 6, 7, 8],
              ['data1', 24.2, 13.4, 10.4, 4.6, 4.1, 4.0, 3.3, 2.1],
          ],
          type: 'bar',
          color: function (color, d) {
              return d.index === 7 ? "#d2d2d2" : "#2b4989";
          },
          labels: {
              format: {
                  data1: d3.format('')
              }
          }
      },
      bar: {
          width: 20 // this makes bar width 100px
      },
      axis: {
        rotated: true,
        y: {
          show: false
        },
        x: {
          tick: {
            outer: false,
          }
        }
      },
      legend: {
        hide: true
      }
  });

  var chart2 = c3.generate({
      bindto: "#chart-2",
      data: {
          x: 'x',
          columns: [
              ['x', 1, 2, 3, 4, 5, 6, 7, 8],
              ['data1', 1.5, -0.5, 0.2, -0.7, 0, 0.9, 1.2, -0.2],
          ],
          type: 'bar',
          color: function (color, d) {
            return d.index === 7 ? "#d2d2d2" : "#2b4989";
          },
          labels: {
            format: {
              data1: d3.format('')
            }
          }
      },
      bar: {
        width: 20 // this makes bar width 100px
      },
      axis: {
        rotated: true,
        y: {
          show: false
        },
        x: {
          show: false,
          tick: {
            outer: false
          }
        }
      },
      legend: {
        hide: true
      },
      grid: {
        y: {
          lines: [
            {value: 0, text: ''},
          ]
        }
      },
  });

  var chart3 = c3.generate({
    bindto: "#chart-3",
    data: {
      x : 'x',
      columns: [
        ['x', '2016', '2017', '2018'],
        ['data1', 0.3, 0.27, 0.25],
        ['data2', 0.6, 0.55, 0.50],
        ['data3', 0.1, 0.18, 0.25],
      ],
      groups: [
        ['data1', 'data2', 'data3']
      ],
      type: 'bar',
      colors: {
        data1: '#617fbd',
        data2: '#405e9e',
        data3: '#2b4989'
      },
      labels: {
        format: {
          data1: d3.format('%'),
          data2: d3.format('%'),
          data3: d3.format('%')
        }
      }
    },
    axis: {
      x: {
        type: 'category' // this needed to load string x value
      },
      y : {
        tick: {
          format: d3.format('%'),
          values: [0,0.5,1]
        },
      }
    },
    legend: {
      hide: true
    },
    grid: {
      lines: {
        front: false
      },
      y: {
        lines: [
          {value: 0},
          {value: .1},
          {value: .2},
          {value: .3},
          {value: .4},
          {value: .5},
          {value: .6},
          {value: .7},
          {value: .8},
          {value: .9},
          {value: 1}
        ]
      }
    }
  });

  d3.select(".c3-texts-data1 .c3-text-0").attr("transform", "translate(0,45)");
  d3.select(".c3-texts-data1 .c3-text-1").attr("transform", "translate(0,40)");
  d3.select(".c3-texts-data1 .c3-text-2").attr("transform", "translate(0,40)");

  d3.select(".c3-texts-data2 .c3-text-0").attr("transform", "translate(0,85)");
  d3.select(".c3-texts-data2 .c3-text-1").attr("transform", "translate(0,75)");
  d3.select(".c3-texts-data2 .c3-text-2").attr("transform", "translate(0,70)");

  d3.select(".c3-texts-data3 .c3-text-0").attr("transform", "translate(0,19)");
  d3.select(".c3-texts-data3 .c3-text-1").attr("transform", "translate(0,29)");
  d3.select(".c3-texts-data3 .c3-text-2").attr("transform", "translate(0,38)");

  d3.select(".chart-wrap-3 .c3-axis-y").attr("transform", "translate(35,-10)");
  
});